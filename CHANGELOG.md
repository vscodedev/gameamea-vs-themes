# ChangeLog

## 1.0.1

- bugfix in the material-theme-ocean-evolved theme filename 
- update readme

## 1.0.0

- Initial release
