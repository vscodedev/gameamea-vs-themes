# About

Contains several dark themes for Visual Studio Code:

- Darcula Meets Markdown: A mix of the [darcula 5 Stars](https://github.com/dobbbri/darcula-5-stars) theme, and some markdown color syntaxing from the "monokai-dimmed" theme
- Material theme ocean evolved: A copy with small changes of the "Material Theme Ocean High Contrast" theme from the [Material Theme extension](https://marketplace.visualstudio.com/items?itemName=Equinusocio.vsc-material-theme)

## Notes

- The `export` folder contains the packed extension that can be installed into Visual Studio code. 
- It has been created on a windows10 x64 system, but it should be compatible with all system that allows Visual studio code to run.

## Authors

Many thanks to the author of the original themes used here.

Created by [Gameamea Studio](http://www.gameamea.com)

**Enjoy!**
